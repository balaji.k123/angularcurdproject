import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { MainPageComponent } from './main-page/main-page.component';
import { ServiceFileService } from './services/service-file.service';
import { EditRecordComponent } from './edit-record/edit-record.component';
import { AdddataComponent } from './adddata/adddata.component';

const router: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },
  {
    path: 'loginpage',
    component: LoginPageComponent,
  },
  {
    path: 'addpage',
    component: AdddataComponent,
  },
  {
    path: 'mainpage',
    component: MainPageComponent,
  },
  {
    path: 'editpage/:id',
    component: EditRecordComponent,
  },
];
@NgModule({
  declarations: [
    DashboardComponent,
    AppComponent,
    LoginPageComponent,
    MainPageComponent,
    EditRecordComponent,
    AdddataComponent,
  ],
  imports: [BrowserModule, RouterModule.forRoot(router), FormsModule],
  providers: [ServiceFileService],
  bootstrap: [AppComponent],
})
export class AppModule {}
