import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ServiceFileService {
  constructor() {}
  auth(): boolean {
    if (sessionStorage.getItem('token') === null) {
      return true;
    }
    return false;
  }
}
export class userObj {
  sno?: string;
  username?: string;
  location?: string;
  phonenumber?: number;
}
