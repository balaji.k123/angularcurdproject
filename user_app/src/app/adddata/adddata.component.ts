import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { userObj } from '../services/service-file.service';

@Component({
  selector: 'app-adddata',
  templateUrl: './adddata.component.html',
  styleUrls: ['./adddata.component.css'],
})
export class AdddataComponent implements OnInit {
  userObj: userObj = new userObj();
  constructor(private router: Router) {
    this.userObj = new userObj();
  }

  ngOnInit(): void {}
  // frist checjk panna
  getNewUserId() {
    const oldRecords = localStorage.getItem('userList');
    if (oldRecords !== null) {
      const userList = JSON.parse(oldRecords);
      return userList.length + 1;
    } else {
      return 1;
    }
  }

  saveUser() {
    const latestId = this.getNewUserId();
    this.userObj.sno = latestId;
    const oldRecords = localStorage.getItem('userList');
    if (oldRecords !== null) {
      const userList = JSON.parse(oldRecords);
      userList.push(this.userObj);
      localStorage.setItem('userList', JSON.stringify(userList));
    } else {
      const userArr = [];
      userArr.push(this.userObj);
      localStorage.setItem('userList', JSON.stringify(userArr));
    }
    this.router.navigateByUrl('/mainpage');
  }
}
