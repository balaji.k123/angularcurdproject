import { Component, OnInit } from '@angular/core';
import { userObj } from '../services/service-file.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css'],
})
export class MainPageComponent implements OnInit {
  userList: userObj[];
  constructor() {
    this.userList = [];
  }

  ngOnInit(): void {
    const records = localStorage.getItem('userList');
    if (records !== null) {
      this.userList = JSON.parse(records);
    }
  }

  // array: any = [
  //   {
  //     name: 'balaji',
  //     location: 'Chennai',
  //     phno: '9940668806',
  //     email: 'balaji',
  //   },
  //   {
  //     name: 'suresh',
  //     location: 'Chennai',
  //     phno: '9940668806',
  //   },
  //   {
  //     name: 'suresh',
  //     location: 'Chennai',
  //     phno: '9940668806',
  //   },
  // ];
  delete(id: any) {
    const oldRecords = localStorage.getItem('userList');
    if (oldRecords !== null) {
      const userList = JSON.parse(oldRecords);

      userList.splice(
        userList.findIndex((a: any) => a.sno === id),
        1
      );

      localStorage.setItem('userList', JSON.stringify(userList));
    }
    const records = localStorage.getItem('userList');
    if (records !== null) {
      this.userList = JSON.parse(records);
    }
  }
}
