import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css'],
})
export class LoginPageComponent implements OnInit {
  // loginArray: any = {};
  userName: any;
  password: any;
  constructor(private route: Router) {}

  ngOnInit(): void {}
  login(): void {
    if (this.userName === 'balaji' && this.password === '000') {
      this.route.navigateByUrl('mainpage');
    } else {
      alert('Plz enter correct Details');
    }
  }

  // login() {
  //   console.log(this.loginArray);

  //   localStorage.setItem('name', JSON.stringify(this.loginArray));
  // }
}
