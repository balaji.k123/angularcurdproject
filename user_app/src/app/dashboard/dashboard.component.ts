import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from '../services/service-file.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  constructor(public oru_name: ServiceFileService) {}

  ngOnInit(): void {}
}
