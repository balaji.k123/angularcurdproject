import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { userObj } from '../services/service-file.service';
@Component({
  selector: 'app-edit-record',
  templateUrl: './edit-record.component.html',
  styleUrls: ['./edit-record.component.css'],
})
export class EditRecordComponent implements OnInit {
  userObj1: userObj;
  constructor(private route: ActivatedRoute, private router: Router) {
    this.userObj1 = new userObj();
    this.route.params.subscribe((res) => {
      this.userObj1.sno = res['id'];
    });
  }
  ngOnInit(): void {
    const oldRecords = localStorage.getItem('userList');
    if (oldRecords !== null) {
      const userList = JSON.parse(oldRecords);
      const currentUser = userList.find((m: any) => m.sno == this.userObj1.sno);
      if (currentUser !== undefined) {
        this.userObj1.username = currentUser.username;
        this.userObj1.location = currentUser.location;
        this.userObj1.phonenumber = currentUser.phonenumber;
      }
    }
  }
  // getNewUseId() {
  //   const oldRecords = localStorage.getItem('userList');
  //   if (oldRecords !== null) {
  //     const userList = JSON.parse(oldRecords);
  //     return userList.length + 1;
  //   } else {
  //     return 1;
  //   }
  // }
  update() {
    const oldRecords = localStorage.getItem('userList');
    if (oldRecords !== null) {
      const userList = JSON.parse(oldRecords);
      userList.splice(
        userList.findIndex((a: any) => a.sno == this.userObj1.sno),
        1
      );
      userList.push(this.userObj1);
      localStorage.setItem('userList', JSON.stringify(userList));
    }
    this.router.navigateByUrl('/mainpage');
  }
}
